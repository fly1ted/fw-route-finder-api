<?php
use App\Stop;
use App\Route;
use App\RouteStop;
use App\MyRoute;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// https://stackoverflow.com/a/35223390
function csvToArray($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }

    return $data;
}

$router->get('/', function () use ($router) {
    return 'Hello';
});

$router->get('/stops', function () use ($router) {
    return response()->json(Stop::get());
});

$router->get('/routes', function () use ($router) {
    return response()->json(Route::with(['stops' => function ($q) {
        $q->orderBy('order', 'asc');
    }])->get());
});

$router->post('/my-routes', function () use ($router) {
    $request = app('request');
    // https://www.codecheef.org/article/avoid-pivot-table-and-use-json-column-in-laravel
    $route = new MyRoute;
    $route->route_id = $request->route_id;
    $route->save();

    return response('OK', 200);
});

$router->get('/my-routes', function () use ($router) {
    return response()->json(MyRoute::get());
});

$router->get('/refresh', function () use ($router) {
    Artisan::call('migrate:fresh', array('--seed' => true, '--force' => true));

    return 'OK';
});

$router->post('/upload', function () use ($router) {
    $request = app('request');
    $file = $request->file('file');

    if ($file->isValid()) {
        $arr = csvToArray($file->getRealPath());
        $stops = [];

        foreach ($arr as $key => $value) {
            $stops = array_merge($stops, explode('-', $value['route']));
        }

        $stops = array_unique($stops);

        foreach ($stops as $key => $value) {
            if(!Stop::where('title', '=', $value)->first()){
                Stop::create(['title' => $value]);
            }
        }

        foreach ($arr as $key => $value) {
            $route = new Route;
            $route->save();

            $arrStops = explode('-', $value['route']);
            $arrDistance = explode('-', $value['distance']);

            foreach ($arrStops as $key2 => $value2) {
                $stop = Stop::where('title', '=', $value2)->first();

                $route->stops()->save($stop, [
                    'distance' => (int) $arrDistance[$key2],
                    'order' => $key2,
                ]);
            }
        }
    }

    return response('OK', 200);
});
