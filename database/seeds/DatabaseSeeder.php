<?php

use Illuminate\Database\Seeder;
use App\Stop;
use App\Route;
use App\RouteStop;

use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stops = array_map(function ($v) {
            $stop = new Stop;
            $stop->title = $v;
            $stop->save();

            return $stop;
        }, ['Kyiv', 'Kharkiv', 'Kherson', 'Odessa']);

        $routes = [
            [$stops[0], $stops[1]],
            [$stops[0],  $stops[2]],
            [$stops[0],  $stops[3]],
            //
            [$stops[0],  $stops[1], $stops[3]],
            [$stops[0],  $stops[2], $stops[3]],
            [$stops[0],  $stops[3], $stops[2]],
            //
            [$stops[1],  $stops[3]],
            [$stops[3],  $stops[0]],
        ];

        foreach ($routes as $key => $value) {
            $route = new Route;

            // $route->title = $key;
            $route->save();

            foreach ($value as $key2 => $value2) {
                $route->stops()->save($value2, [
                    'distance' => $key2 * rand(2, 50),
                    'order' => $key2,
                ]);
            }
            // $route->stops()->saveMany($value);
            // $route->stops()->updateExistingPivot($value);
        }
    }
}
