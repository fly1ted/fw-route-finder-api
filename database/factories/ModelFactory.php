<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Stop;
use App\Route;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Stop::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
    ];
});

$factory->define(Route::class, function (Faker $faker) {
    return [
        'title' => '',
    ];
});
