<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stops', function (Blueprint $table) {
            $table->id();
            $table->string('title');
        });

        Schema::create('routes', function (Blueprint $table) {
            $table->id();
            // $table->string('title');
            // $table->integer('distance');
            // $table->unsignedBigInteger('stop_id');
            // $table->foreign('stop_id')->references('id')->on('stops');
        });

        Schema::create('route_stop', function (Blueprint $table) {
            $table->unsignedBigInteger('route_id');
            $table->unsignedBigInteger('stop_id');
            $table->integer('distance');
            $table->integer('order');

            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('stop_id')->references('id')->on('stops');
        });

        Schema::create('my_routes', function (Blueprint $table) {
            $table->id();
            $table->text('route_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stops');
        Schema::dropIfExists('routes');
        Schema::dropIfExists('route_stop');
        Schema::dropIfExists('my_routes');
    }
}
