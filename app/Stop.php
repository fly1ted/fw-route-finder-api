<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stop extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    public function routes()
    {
        return $this
            ->belongsToMany(Route::class)
            // ->as('routes')
            ->using(RouteStop::class);
    }
}
