<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RouteStop extends Pivot
{
    public $timestamps = false;

    // https://stackoverflow.com/a/46806137
    protected $casts = [
        'route_id' => 'integer',
        'stop_id' => 'integer',
        'order' => 'integer',
        'distance' => 'integer'
    ];
}
