<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyRoute extends Model
{
    protected $casts = [
        'route_id' => 'array'
    ];
}
