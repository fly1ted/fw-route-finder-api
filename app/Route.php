<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'title',
    // ];

    public function stops()
    {
        return $this
            ->belongsToMany(Stop::class)
            ->using(RouteStop::class)
            // ->as('stops')
            ->withPivot([
                'order',
                'distance'
            ]);
    }
}
